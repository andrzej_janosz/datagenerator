package com.company;

import java.io.*;
import java.text.*;
import java.util.*;

public class Main {
    static Random random=new Random();
    public static void main(String[] args) throws IOException, ParseException {
        // write your code here
        File fout = new File("C:\\out.txt");
        FileOutputStream fos = new FileOutputStream(fout);
        String stockName=new String("CYBATON");
        String startingHour="080000";
        String startingDay="20150101";
        String date;
        DateFormat datos = new SimpleDateFormat("yyyyMMddHHmmss");
        Calendar gc=new GregorianCalendar();
        Date d=datos.parse(startingDay+startingHour);
        gc.setTime(d);
        boolean isDirectionUp=true;
        double startRangeMin=1;
        double startRangeMax=5;
        DecimalFormat decimalFormat=new DecimalFormat("##.##");
        DecimalFormatSymbols dfs=new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        decimalFormat.setDecimalFormatSymbols(dfs);


        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        for (int i = 0; i < 100; i++) {
            gc.add(Calendar.SECOND, 1);
            date=datos.format(gc.getTime());
            String dateToWrite=date.substring(0,8);
            String hourToWrite=date.substring(8, 14);
            double[] ohlc=Main.getOhlc(startRangeMin, startRangeMax);
            double[] sorted=Main.getSortedOhlc(ohlc);
            double high=sorted[3];
            double low=sorted[0];
            double open=0;
            double close=0;
            if(isDirectionUp) {
                if(sorted[1]>sorted[2]) {
                    open=sorted[2];
                    close=sorted[1];
                }
                else {
                    open=sorted[1];
                    close=sorted[2];
                }
                startRangeMin++;
                startRangeMax++;
            }
            else {
                if(sorted[1]>sorted[2]) {
                    open=sorted[1];
                    close=sorted[2];
                }
                else {
                    open=sorted[2];
                    close=sorted[1];
                }
                startRangeMin--;
                startRangeMax--;
            }

            if((i%23==0) && (i!=0)) {
                isDirectionUp=!isDirectionUp;
            }
            bw.write(stockName + "," + "0" + "," + dateToWrite + "," + hourToWrite + "," + decimalFormat.format(open) + "," + decimalFormat.format(high) + "," + decimalFormat.format(low) + "," + decimalFormat.format(close));
            bw.newLine();
            System.out.println(isDirectionUp);
        }

        bw.close();
    }

    public static double randomInRange(double min, double max) {
        double range = max - min;
        double scaled = random.nextDouble() * range;
        double shifted = scaled + min;
        return shifted; // == (rand.nextDouble() * (max-min)) + min;
    }

    public static double[] getOhlc(double min, double max) {
        double[] ohlc=new double[4];
        for(int i=0;i<4;i++) {
            ohlc[i]=Main.randomInRange(min, max);
        }
        return ohlc;
    }

    public static double[] getSortedOhlc(double[] ohlc) {
        Arrays.sort(ohlc);
        return ohlc;
    }
}
